import { applyDecorators, Headers, CanActivate, ExecutionContext } from '@nestjs/common';

export class Authentication implements CanActivate {
    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        const authHead = request.headers.authorization;
        if (!authHead) {
            return false;
        }

        const authArr = authHead.split(" ")
        if (authArr.length != 2) {
            return false;
        }

        

        console.log(authHead);

        return true;
    }
};


