import { Posts, PostsDocument } from './posts.model';
import { InjectModel } from '@nestjs/mongoose';
import { Injectable, forwardRef, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { PostStatus, FilterMany } from './posts.type';
import { UserService } from '@modules/user/user.service';
import { ObjectID } from 'bson';

@Injectable()
export class PostsService {
  constructor(
    @InjectModel(Posts.name)
    private postsModel: Model<PostsDocument>,
    @Inject(forwardRef(() => UserService))
    private userService: UserService,
  ) {}

  /**
   * for creating post
   * @param post Posts
   */
  public async createPost(post: Posts) {
    try {
      post.slug = post.title.replace(/ /g, '-');
      post.status = PostStatus.ACTIVE;
      const result = await new this.postsModel(post).save();
      return await this.userService.addPost(
        result._id,
        result.author.toHexString(),
      );
    } catch (err) {
      return Promise.reject(err);
    }
  }

  /**
   * get more than one posts with pagination
   * @param param FilterMany
   */
  public async getMany(param: FilterMany) {
    try {
      let filter: any = [];

      // adding filter to the query
      if (param.filter != {}) {
        filter.push({
          $match: param.filter,
        });
      }

      // merging user model
      filter.push({
        $lookup: {
          form: 'users',
          localField: 'author',
          foreignField: '_id',
          as: 'author',
        },
      });

      // sorting
      let sort: any = {
        $sort: {
          createdAt: 1,
        },
      };
      if (!!param.sort) {
        let sortArr = param.sort.split('|');
        const computeSort = (val: number) => ({
          $sort: {
            [sortArr[0]]: val,
          },
        });
        if (/A/.test(sortArr[1])) {
          sort = computeSort(1);
        } else {
          sort = computeSort(-1);
        }
      }
      filter.push(sort);

      // pagination
      let page = !!param.page ? param.page : 1;
      let limit = !!param.limit ? param.limit : 20;
      let skip = 0;
      skip = page * limit - limit;
      filter = [...filter, { $skip: skip }, { $limit: limit }];

      if (!!param.fields) {
        let fieldArr = param.fields.split(',');
        if (!!fieldArr.length) {
          let fields = {};
          fieldArr.forEach(res => (fields[res] = 1));
          filter.push({
            $project: fields,
          });
        }
      }

      const result = await this.postsModel.aggregate(filter);
      return result;
    } catch (err) {
      return Promise.reject(err);
    }
  }

  /**
   * getOne post using slug
   * @param slug string
   */
  public async getOne(slug: string) {
    try {
      const result = await this.getMany({
        filter: { slug: slug },
        limit: 1,
      });
      if (!!result.length) {
        return result[0];
      }
      return Promise.reject('Post not found');
    } catch (err) {
      return Promise.reject(err);
    }
  }

  public async addComment(postId: string, commentId: string) {
    try {
      const pId = new ObjectID(postId);
      const cId = new ObjectID(commentId);
      return this.postsModel.findOneAndUpdate(
        { _id: pId },
        {
          $addToSet: {
            comments: cId,
          },
        },
      );
    } catch (err) {
      return Promise.reject(err);
    }
  }
}
