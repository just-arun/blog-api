import { Posts } from './posts.model';
import { PostsService } from './posts.service';
import { Controller, Get, Query, Res, Post, Body, Param } from '@nestjs/common';
import { Response } from 'express';

@Controller('posts')
export class PostsController {
  constructor(private postsService: PostsService) {}

  @Get('/')
  async getAll(
    @Res() res: Response,
    @Query('filter') filter?: string,
    @Query('limit') limit?: number,
    @Query('page') page?: number,
    @Query('fields') fields?: string,
    @Query('sort') sort?: string,
  ) {
    try {
      let param: any = {
        filter: !!filter ? filter : {},
        limit: limit,
        page: page,
        fields: fields,
        sort: sort,
      };
      const posts = await this.postsService.getMany(param);
      return { posts };
    } catch (err) {
      res.status(500).json({ error: err });
    }
  }

  @Post('/')
  async create(@Body() post: Posts, @Res() res: Response) {
    try {
      const result = await this.postsService.createPost(post);
      return { result };
    } catch (err) {
      res.status(500).json({ error: err });
    }
  }

  @Get('/:slug')
  async getOne(@Param('slug') slug: string, @Res() res: Response) {
    try {
      const post = await this.postsService.getOne(slug);
      return { post };
    } catch (err) {
      res.status(500).json({ error: err });
    }
  }
}
