import { PostStatus } from './posts.type';
import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { ObjectID } from 'bson';
import { set, Document } from 'mongoose';
import AppConfig, { NodeEnv } from '@config/app.config';

export type PostsDocument = Posts & Document;

@Schema({
  timestamps: {
    createdAt: true,
    updatedAt: true,
  },
})
export class Posts {
  @Prop({ required: true, unique: true })
  title: string;

  @Prop({ required: true, unique: true })
  slug: string;

  @Prop({ required: true, unique: true })
  description: string;

  @Prop({ required: true })
  keywords: string;

  @Prop({ required: true, type: Number })
  status: PostStatus;

  @Prop({
    required: true,
    type: [ObjectID],
    ref: 'comments',
    default: new Array(0),
  })
  comments: ObjectID[];

  @Prop({ type: ObjectID, ref: 'users' })
  author: ObjectID;
}

if (AppConfig.NODE_ENV == NodeEnv.LOCAL) {
  set('debug', true);
}

export const PostsSchema = SchemaFactory.createForClass(Posts);
