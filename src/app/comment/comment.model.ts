
import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { ObjectID } from 'bson';
import { set, Document } from 'mongoose';
import AppConfig, { NodeEnv } from '@config/app.config';

export type CommentsDocument = Comments & Document;

@Schema({
  timestamps: {
    createdAt: true,
    updatedAt: true,
  },
})
export class Comments {
  @Prop({ type: String, required: true })
  message: string;

  @Prop({ type: ObjectID, required: true })
  post: ObjectID;

  @Prop({ type: ObjectID, ref: 'users' })
  author: ObjectID;
}

if (AppConfig.NODE_ENV == NodeEnv.LOCAL) {
  set('debug', true);
}

export const CommentsSchema = SchemaFactory.createForClass(Comments);
