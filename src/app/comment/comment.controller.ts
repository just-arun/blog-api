import { Response } from 'express';
import { Comments } from './comment.model';
import { CommentService } from './comment.service';
import { Controller, Post, Body, Res, Get } from '@nestjs/common';

@Controller('comment')
export class CommentController {
  constructor(private commentService: CommentService) {}

  @Post('/')
  async create(@Body() body: Comments, @Res() res: Response) {
    try {
      const comment = await this.commentService.createComment(body);
      return res.status(202).json({ comment });
    } catch (err) {
      return res.status(500).json({ error: err });
    }
  }

  @Get('/')
  async getMany(@Res() res: Response) {
    try {
    } catch (err) {
      return res.status(500).json({ error: err });
    }
  }

  @Get('/:id')
  async getOne(@Res() res: Response) {
    try {
    } catch (err) {
      return res.status(500).json({ error: err });
    }
  }
}
