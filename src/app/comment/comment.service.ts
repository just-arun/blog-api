import { FilterMany } from './comment.type';
import { UserService } from '@modules/user/user.service';
import { PostsService } from './../posts/posts.service';
import { Comments, CommentsDocument } from './comment.model';
import { Injectable, forwardRef, Inject } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class CommentService {
  constructor(
    @InjectModel(Comments.name)
    private commentModel: Model<CommentsDocument>,
    @Inject(forwardRef(() => PostsService))
    private postService: PostsService,
    @Inject(forwardRef(() => UserService))
    private userService: UserService,
  ) {}

  /**
   * create comment using this method it will also
   * update post and user collection with their respective id
   * @param comment Comments
   */
  public async createComment(comment: Comments) {
    try {
      const result = await new this.commentModel(comment).save();
      await this.postService.addComment(comment.post.toHexString(), result._id);
      await this.userService.addComment(
        comment.author.toHexString(),
        result._id,
      );
      return result;
    } catch (err) {
      return Promise.reject(err);
    }
  }

  public async getMany(param: FilterMany) {
    try {
      let filter: any = [];

      // adding filter to the query
      if (param.filter != {}) {
        filter.push({
          $match: param.filter,
        });
      }

      // sorting
      let sort: any = {
        $sort: {
          createdAt: 1,
        },
      };
      if (!!param.sort) {
        let sortArr = param.sort.split('|');
        const computeSort = (val: number) => ({
          $sort: {
            [sortArr[0]]: val,
          },
        });
        if (/A/.test(sortArr[1])) {
          sort = computeSort(1);
        } else {
          sort = computeSort(-1);
        }
      }
      filter.push(sort);

      // pagination
      let page = !!param.page ? param.page : 1;
      let limit = !!param.limit ? param.limit : 20;
      let skip = 0;
      skip = page * limit - limit;
      filter = [...filter, { $skip: skip }, { $limit: limit }];

      if (!!param.fields) {
        let fieldArr = param.fields.split(',');
        if (!!fieldArr.length) {
          let fields = {};
          fieldArr.forEach(res => (fields[res] = 1));
          filter.push({
            $project: fields,
          });
        }
      }
    } catch (err) {
      return Promise.reject(err);
    }
  }
}
