import { LoginDto } from './auth.dto';
import { Response } from 'express';
import { AuthService } from './auth.service';
import { Controller, Post, Res, Body } from '@nestjs/common';

@Controller('auth')
export class AuthController {
    constructor(
        private authService: AuthService,
    ) {}

    @Post("/login")
    async login(
        @Res() res: Response,
        @Body() user: LoginDto,
    ) {
        try {
            const result = await this.authService
            .login(user);
            res.set("x-access", result.access)
            return res.status(200)
            .json({ data: result.user })
        } catch (err) {
            return res.status(500).json({error: err});
        }
    }
}
