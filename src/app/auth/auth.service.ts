import { AES, enc } from 'crypto-js';
import { LoginDto } from './auth.dto';
import { Injectable, forwardRef, Inject } from '@nestjs/common';
import { UserService } from '@modules/user/user.service';
import AppConfig, { NodeEnv } from '@config/app.config';
import * as jwt from 'jsonwebtoken'


@Injectable()
export class AuthService {
    constructor(
        @Inject(forwardRef(() => UserService))
        private userService: UserService,
    ) {}

    public async login(user: LoginDto) {
        try {
            const dbUser = await this.userService
            .getRawData({
                email: user.email
            }, { password: 1, userName: 1, _id: 1 });

            
            if (!!!dbUser) {
                return Promise.reject("user not found")
            }

            if (!(this.comparePwd(dbUser.password, user.password))) {
                return Promise.reject("Invalid Credentials");
            }

            const accessToken = await this.createToken(
                { id: dbUser._id },
                AppConfig.APP_SECRET,
                AppConfig.ACCESS_TOKEN
            );

            return {
                access: accessToken,
                user: {
                    name: dbUser.userName,
                    _id: dbUser._id,
                }
            }
            
        } catch (err) {
            return Promise.reject(err)
        }
    }

    public comparePwd(dbPwd, userPwd) {
        try {
            var bytes = AES.decrypt(dbPwd, AppConfig.APP_SECRET);
            var decryptedData = JSON.parse(bytes.toString(enc.Utf8));
            return decryptedData.pwd == userPwd;
        } catch (err) {
            console.log(err);
            return false;
        }
    }

    public async createToken(payload: any, secret: string, expiresIn: string) {
        try {
            let token = jwt.sign(
                payload, secret, 
                { expiresIn }
            );
            if (AppConfig.NODE_ENV == NodeEnv.LOCAL) {
                token = 'Bearer ' +  token;
            }
            return token;
        } catch (err) {
            return Promise.reject(err);
        }
    }
}
