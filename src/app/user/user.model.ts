import AppConfig, { NodeEnv } from '@config/app.config';
import { UserType } from './user.type';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, set } from 'mongoose';
import { ObjectID } from 'bson';

export type UserDocument = User & Document;

@Schema({
  timestamps: {
    createdAt: true,
    updatedAt: true,
  },
})
export class User {
  @Prop({ required: true, unique: true })
  userName: string;

  @Prop({ required: true, unique: true })
  email: string;

  @Prop({ required: true })
  password: string;

  @Prop({ required: true })
  type: UserType;

  @Prop({ type: [ObjectID], ref: 'posts', default: new Array(0) })
  posts: ObjectID[];

  @Prop({ type: [ObjectID], ref: 'comments', default: new Array(0) })
  comments: ObjectID[];
}

if (AppConfig.NODE_ENV == NodeEnv.LOCAL) {
  set('debug', true);
}

export const UserSchema = SchemaFactory.createForClass(User);
