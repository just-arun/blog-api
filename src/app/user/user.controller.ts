import { Authentication } from './../../decorators/authentication.decorator';
import { User } from './user.model';
import { UserService } from './user.service';
import { Controller, Get, Query, Post, Body, Res, Param, UseGuards } from '@nestjs/common';
import { Response } from 'express';
// import { FilterMany } from './user.type';

@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @Get('/')
  @UseGuards(new Authentication())
  async getUsers(
    @Query('filter') filter?: string,
    @Query('limit') limit?: number,
    @Query('page') page?: number,
    @Query('fields') fields?: string,
    @Query('sort') sort?: string,
  ) {
    let param: any = {
      filter: !!filter ? filter : {},
      limit: limit,
      page: page,
      fields: fields,
      sort: sort,
    };
    try {
      const result = await this.userService.getMany(param);
      return { users: result };
    } catch (err) {
      return Promise.reject(err);
    }
  }

  @Post('/')
  async create(@Body() user: User, @Res() res: Response) {
    try {
      const result = await this.userService.create(user);
      return res.status(200).json({ user: result });
    } catch (err) {
      return res.status(500).json({ error: err });
    }
  }

  @Get('/:id')
  async getOne(@Param('id') id: string, @Res() res: Response) {
    try {
      const user = await this.userService.getOne(id);
      return { user };
    } catch (err) {
      return res.status(404).json({ error: err });
    }
  }
}
