export enum UserType {
  User,
  Admin,
}

export interface FilterMany {
  filter?: any;
  sort?: string;
  fields?: string;
  page?: number;
  limit?: number;
  skip?: number;
}
