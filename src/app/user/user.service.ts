import { AES } from 'crypto-js';
import { ObjectID } from 'bson';

import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import AppConfig from '@config/app.config';
import { User, UserDocument } from './user.model';
import { FilterMany, UserType } from './user.type';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name)
    private userModel: Model<UserDocument>,
  ) {}

  /**
   * for getting one user
   * @param id string
   */
  public async getOne(id: string) {
    try {
      let _id = new ObjectID(id);
      let result = await this.getMany({ filter: { _id }, limit: 1 });
      if (typeof result[0] != 'undefined') {
        return result[0];
      }
      return Promise.reject(new Error('User not found'));
    } catch (err) {
      return Promise.reject(err);
    }
  }

  /**
   * for getting more than one user
   */
  public async getMany(param: FilterMany): Promise<User[]> {
    try {
      let filter: any = [];

      // adding filter to the query
      if (param.filter != {}) {
        filter.push({
          $match: param.filter,
        });
      }

      // sorting
      let sort: any = {
        $sort: {
          createdAt: 1,
        },
      };
      if (!!param.sort) {
        let sortArr = param.sort.split('|');
        const computeSort = (val: number) => ({
          $sort: {
            [sortArr[0]]: val,
          },
        });
        if (/A/.test(sortArr[1])) {
          sort = computeSort(1);
        } else {
          sort = computeSort(-1);
        }
      }
      filter.push(sort);

      // pagination
      let page = !!param.page ? param.page : 1;
      let limit = !!param.limit ? param.limit : 20;
      let skip = 0;
      skip = page * limit - limit;
      filter = [...filter, { $skip: skip }, { $limit: limit }];

      if (!!param.fields) {
        let fieldArr = param.fields.split(',');
        if (!!fieldArr.length) {
          let fields = {};
          fieldArr.forEach(res => (fields[res] = 1));
          filter.push({
            $project: fields,
          });
        }
      }

      const result = await this.userModel.aggregate(filter).then(res =>
        res.map(itm => {
          delete itm.password;
          return itm;
        }),
      );
      return result;
    } catch (err) {
      return Promise.reject(err);
    }
  }

  /**
   * for creating one user
   */
  public async create(user: User) {
    const dbUser = await this.getMany({
      filter: {
        $or: [{ email: user.email }, { userName: user.userName }],
      },
      limit: 1,
    });
    if (!!dbUser.length) {
      return Promise.reject('User already exist');
    }
    user.password = AES.encrypt(JSON.stringify({pwd: user.password}), AppConfig.APP_SECRET).toString();
    user.type = UserType.User;
    return new this.userModel(user)
      .save()
      .then(res => ({ _id: res._id }))
      .catch(err => Promise.reject(err));
  }

  /**
   * for updating one user
   * @param id string
   */
  public async update(id: string, user: User) {
    try {
      const _id = new ObjectID(id);
      return this.userModel
        .findOneAndUpdate(
          { _id },
          { $set: user },
          {
            fields: {
              userName: 1,
            },
          },
        )
        .exec();
    } catch (err) {
      return Promise.reject(err);
    }
  }

  /**
   * adding post id to the user collection
   * @param userId string
   * @param postId string
   */
  public async addPost(userId: string, postId: string) {
    try {
      const uId = new ObjectID(userId);
      const pId = new ObjectID(postId);
      return await this.userModel
        .updateOne(
          { _id: uId },
          {
            $addToSet: {
              posts: pId,
            },
          },
        )
        .lean()
        .exec();
    } catch (err) {
      return Promise.reject(err);
    }
  }

  /**
   * for adding comment id in user for mapping users
   * @param userId string
   * @param commentId string
   */
  public async addComment(userId: string, commentId: string) {
    try {
      const uId = new ObjectID(userId);
      const cId = new ObjectID(commentId);
      return await this.userModel
        .updateOne(
          { _id: uId },
          {
            $addToSet: {
              comments: cId,
            },
          },
        )
        .lean()
        .exec();
    } catch (err) {
      return Promise.reject(err);
    }
  }

  /**
   * for accessing user data from outside
   * @param query any
   * @param project any
   */
  public async getRawData(query: any, project?: any) {
    return await this.userModel
    .findOne(query, project)
  }
}
