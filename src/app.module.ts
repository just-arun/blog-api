import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from '@modules/user/user.module';
import { PostsModule } from './app/posts/posts.module';
import { CommentModule } from './app/comment/comment.module';
import { AuthModule } from './app/auth/auth.module';
import { CommonModule } from './app/common/common.module';
import AppConfig from '@config/app.config';

@Module({
  imports: [
    MongooseModule.forRoot(AppConfig.MONGO_URI, { autoIndex: false }),
    UserModule,
    PostsModule,
    CommentModule,
    AuthModule,
    CommonModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
