import { config } from 'dotenv';
import { Tracing } from 'trace_events';

export enum NodeEnv {
  DEV = 'development',
  PROD = 'production',
  LOCAL = 'local'
}

class Config {
  PORT!: number;
  MONGO_URI!: string;
  APP_SECRET!: string;
  NODE_ENV!: NodeEnv;
  ACCESS_TOKEN!: string;

  constructor() {
    config();
    let env: any = process.env;
    this.PORT = env.PORT;
    this.MONGO_URI = env.MONGO_URI;
    this.APP_SECRET = env.APP_SECRET;
    this.NODE_ENV = env.NODE_ENV;
    this.ACCESS_TOKEN = env.ACCESS_TOKEN;
  }
}

const AppConfig = new Config();

export default AppConfig;
